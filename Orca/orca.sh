#!/bin/bash
## ORCA -- CLI helper for Docker-Compose application projects
## Author: Prishalan Naidoo <prishalan.naidoo@gmail.com>
## Created: 09 November 2017
## Last Updated: 21 November 2017


# ============= Output colouring ==================================================================
RED='\033[1;31m'
GRN='\033[1;32m'
CYN='\033[0;36m'
CYB='\033[1;36m'
YEL='\033[0;33m'
YEB='\033[1;33m'
BLD='\033[1m'
DEF='\033[0m'


# ============= Script Variables ==================================================================
ORCA="${CYN}ORCA ::${DEF} "
READ="${YEL}$USER >> "
LINE="${CYB}-------------------------------------------------------------------------------------------------${DEF}"
SORRY="${RED}:(${DEF}"
HAPPY="${GRN}:)${DEF}"
root_path=/home/$USER/Sites/
host_prefix="dev"
host_suffix="co.za"


# ============= Script Methods ====================================================================
# === Initialise a freshly cloned docker-compose bare-bones project
init() {
    if [ ! -z "$1" ]; then
        path=$(pwd)/$1
        initappname=$1
    else
        path=$(pwd)
        echo -e  "${ORCA}What is the project's app name? (in lowercase and no spaces, please)"
        echo -en "${READ}"
        read initappname
    fi

    if [ ! -f "${path}/.needsinit" ]; then
        echo -en "${ORCA}Looks as though your project has already been initialised $(printf '\056%.0s' {1..100})" | head -c 100 ; echo -e "..... ${HAPPY}"
        echo -e ${LINE}
        exit 0;
    else
        # Remove Git references and placeholder files
        echo -en "${ORCA}Removing Git references and placeholder files $(printf '\056%.0s' {1..100})" | head -c 100
        find ${path}/ -name ".placeholder" -exec rm -rf {} \;
        rm -rf ${path}/.git ${path}/.gitignore
        rm ${path}/.needsinit
        echo -e " Done ${HAPPY}"
        # Change appname in Nginx or Apache config
        if [ -f ${path}/configurations/apache-default-site.conf ]; then
            echo -en "${ORCA}Changing Apache reference $(printf '\056%.0s' {1..100})" | head -c 100
            sed -i '' "s/changeme/${initappname}/g" ${path}/configurations/apache-default-site.conf;
            echo -e " Done ${HAPPY}"
        fi
        if [ -f ${path}/configurations/nginx-default-site.conf ]; then
            echo -en "${ORCA}Changing Nginx reference $(printf '\056%.0s' {1..100})" | head -c 100
            sed -i '' "s/changeme/${initappname}/g" ${path}/configurations/nginx-default-site.conf;
            echo -e " Done ${HAPPY}"
        fi
        # Change appname in Docker .env file
        echo -en "${ORCA}Changing values in environment file $(printf '\056%.0s' {1..100})" | head -c 100
        sed -i '' "s/changeme/${initappname}/g" ${path}/.env;
        sed -i '' "s/USER_UID=1000/USER_UID=${UID}/g" ${path}/.env;
        sed -i '' "s/USER_GID=1000/USER_GID=${GID}/g" ${path}/.env;
        echo -e " Done ${HAPPY}"
        # Add entry to hosts file
        echo -en "${ORCA}I need elevated privileges to add domain entry to hosts file"
        sudo -- sh -c -e "echo '127.0.0.1	${host_prefix}.${initappname}.${host_suffix}' >> /etc/hosts";
        echo -en "${ORCA}Adding domain entry to hosts file $(printf '\056%.0s' {1..100})" | head -c 100 ; echo -e " Done ${HAPPY}"
        echo -e "${ORCA}${GRN}Right, all done! You can now:${DEF}"
        if [ ! -z "$1" ]; then
            echo -e "        cd ${initappname}/"
        fi
        echo -e "        http://${host_prefix}.${initappname}.${host_suffix}/"
        echo -e "${LINE}"
        exit 0
    fi
}


# === Create a new Docker-Compose bare-bones project pulled from Git repo
bootstrap() {
    # Check if there is a docker-compose.yml file in the current path
    if [ -f './docker-compose.yml' ]; then
        echo -en "${ORCA}Sorry, a docker project already exists in the current path $(printf '\056%.0s' {1..100})" | head -c 100 ; echo -e ". Bye ${SORRY}"
        echo -e "${LINE}"
        exit 1;
    fi
    # Else, all is well ... continue
    echo -e  "${ORCA}Awesome! Which docker project do you want?"
    echo     "        (1) Magento 1"
    echo     "        (2) Magento 2"
    echo     "        (3) Drupal 7"
    echo     "        (4) Drupal 8"
    echo     "        (5) Laravel 5"
    echo     "        (6) Generic PHP 7.1 App (Apache)"
    echo     "        (7) Generic PHP 7.1 App (Apache, no DB)"
    echo     "        (8) Generic PHP 7.1 App (Nginx)"
    echo     "        (9) Generic PHP 7.1 App (Nginx, no DB)"
    echo -en "${READ}"
    read dockertype
    case ${dockertype} in 
        1)  proj[0]="Magento 1"
            proj[1]="git@bitbucket.org:prishalandev/docker-magento1.git"
            ;;
        2)  proj[0]="Magento 2"
            proj[1]="git@bitbucket.org:prishalandev/docker-magento2.git"
            ;;
        3)  proj[0]="Drupal 7"
            proj[1]="git@bitbucket.org:prishalandev/docker-drupal7.git"
            ;;
        4)  proj[0]="Drupal 8"
            proj[1]="git@bitbucket.org:prishalandev/docker-drupal8.git"
            ;;
        5)  proj[0]="Laravel 5"
            proj[1]="git@bitbucket.org:prishalandev/docker-laravel5.git"
            ;;
        6)  proj[0]="Generic PHP 7.1 App (Apache)"
            proj[1]="git@bitbucket.org:prishalandev/docker-generic-php-apache.git"
            ;;
        7)  proj[0]="Generic PHP 7.1 App (Apache, no DB)"
            proj[1]="git@bitbucket.org:prishalandev/docker-php-apache-no-db.git"
            ;;
        8)  proj[0]="Generic PHP 7.1 App (Nginx)"
            proj[1]="git@bitbucket.org:prishalandev/docker-php-nginx.git"
            ;;
        9)  proj[0]="Generic PHP 7.1 App (Nginx, no DB)"
            proj[1]="git@bitbucket.org:prishalandev/docker-php-nginx-no-db.git"
            ;;
        *)  proj[0]="unknown"
            proj[1]=""
            ;;
    esac
    if [ "${proj[0]}" = "unknown" ]; then
        echo -en "${ORCA}Sorry, I don't understand that option $(printf '\056%.0s' {1..100})" | head -c 100 ; echo -e ". Bye ${SORRY}"
        echo -e "${LINE}"
        exit 1;
    else
        echo -e  "${ORCA}What is the project's app name? (in lowercase and no spaces, please)"
        echo -en "${READ}"
        read appname
        if grep -q "${host_prefix}.${appname}.${host_suffix}" /etc/hosts; then
            echo -en "${ORCA}Sorry, I found a project with the same name $(printf '\056%.0s' {1..100})" | head -c 100 ; echo -e ". Bye ${SORRY}"
            echo -e "${LINE}"
            exit 1;
        else
            echo -en "${ORCA}Great! Fetching ${proj[0]} skeleton from repo $(printf '\056%.0s' {1..100})" | head -c 100
            git clone -q ${proj[1]} ${appname}
            echo -e " Done ${HAPPY}"
            init ${appname}
        fi
    fi
}


# === Get info on Docker-Compose Containers
info() {
    unbuilt_test="Name   Command   State   Ports 
------------------------------"

    if [[ $(docker-compose ps) == "${unbuilt_test}" ]]; then
        echo -en "${ORCA}Hmm, it would appear that this docker project hasn't been built or run as yet $(printf '\056%.0s' {1..100})" | head -c 100 ; echo -e "..... ${SORRY}"
        echo -en "${ORCA}Run 'docker-compose build' and then 'docker-compose up -d' $(printf '\056%.0s' {1..100})" | head -c 100 ; echo -e ".... Bye"
        echo -e "${LINE}"
        exit 1;
    else
        . ./.env
        printf "%-60s%b%37s%b\n" "Project Name" "${BLD}" "${COMPOSE_PROJECT_NAME}" "${DEF}"
        printf "%-60s%b%37s%b\n" "Web URL" "${BLD}" "http://${host_prefix}.${COMPOSE_PROJECT_NAME}.${host_suffix}/" "${DEF}"

        # Get Project mcro-services container info
        (docker-compose ps | grep '^'${COMPOSE_PROJECT_NAME}'.*$') | while read -r line; do
            if [[ ! ${line%% *} =~ ^("${COMPOSE_PROJECT_NAME}_applications_1"|"${COMPOSE_PROJECT_NAME}_data-web_1"|"${COMPOSE_PROJECT_NAME}_data-db_1")$ ]]; then
                service=${line%% *}
                title=$(docker inspect --format '{{ index .Config.Labels "com.docker.compose.mytitle" }}' ${service})
                status=$(docker inspect --format '{{ index .State.Status }}' ${service})
                ip=$(docker inspect --format '{{ .NetworkSettings.Networks.'${COMPOSE_PROJECT_NAME}'_default.IPAddress }}' ${service})
                ports=$(docker inspect --format '{{ index .Config.ExposedPorts }}' ${service})
                if [ ${status} == "running" ]; then
                    status_colour="${GRN}"
                else
                    status_colour="${RED}"
                fi
                echo ""
                echo -e "${YEB}${title}${DEF}"
                printf "%-60s%b%37s%b\n" "Container Name" "${BLD}" "${service}" "${DEF}"
                printf "%-60s%b%37s%b\n" "Container Status" "${status_colour}" "${status}" "${DEF}"
                printf "%-60s%b%37s%b\n" "Container IP Address" "${BLD}" "${ip}" "${DEF}"
                printf "%-60s%b%37s%b\n" "Container Exposed Ports" "${BLD}" "${ports}" "${DEF}"

                if [[ ${service} =~ ^("${COMPOSE_PROJECT_NAME}_mysql_1"|"${COMPOSE_PROJECT_NAME}_mariadb_1")$ ]]; then
                    printf "%-60s%b%37s%b\n" "Database Name" "${BLD}" "${MYSQL_DATABASE}" "${DEF}"
                    printf "%-60s%b%37s%b\n" "Database User" "${BLD}" "${MYSQL_USER}" "${DEF}"
                    printf "%-60s%b%37s%b\n" "Database Pass" "${BLD}" "${MYSQL_PASSWORD}" "${DEF}"
                fi
            fi
        done
    fi

    echo -e "${LINE}"
    exit 0;
}


# === Show Container Statistics
stats() {
    docker stats $(docker ps --format={{.Names}} --filter "label=com.docker.compose.project=${COMPOSE_PROJECT_NAME}")
}


# === Enter Workspace Container
ws() {
    . ./.env
    local workspace=$1
    local container=${COMPOSE_PROJECT_NAME}_${workspace}_1

    if [[ $(docker-compose ps | grep "${container}" ) ]]; then
        status=$(docker inspect --format '{{ index .State.Status }}' ${container})
            docker-compose exec -u www-data ${workspace} ${@:2}
        if [ ${status} == "running" ]; then
            echo -e "${LINE}"
            exit 0;
        else
            echo -en "${ORCA}Um, you need to start the containers before I can access the Workspace $(printf '\056%.0s' {1..100})" | head -c 100 ; echo -e ". Bye ${SORRY}"
            echo -e "${LINE}"
            exit 1;
        fi
    else
        echo -en "${ORCA}Sorry, no ${workspace} container found $(printf '\056%.0s' {1..100})" | head -c 100 ; echo -e ". Bye ${SORRY}"
        echo -e "${LINE}"
        exit 1;
    fi
}


# === Start all containers
start() {
    if [ ! -f './docker-compose.yml' ]; then
        echo -en "${ORCA}Um, I couldn't find a Docker-Compose project in this directory $(printf '\056%.0s' {1..100})" | head -c 100 ; echo -e "..... ${SORRY}"
        echo -e ${LINE}
        exit 1;
    else
        docker-compose up -d
    fi
}


# === Stop all containers
stop() {
    if [ ! -f './docker-compose.yml' ]; then
        echo -en "${ORCA}Um, I couldn't find a Docker-Compose project in this directory $(printf '\056%.0s' {1..100})" | head -c 100 ; echo -e "..... ${SORRY}"
        echo -e ${LINE}
        exit 1;
    else
        docker-compose stop
    fi
}


# === Restart all containers
restart() {
    if [ ! -f './docker-compose.yml' ]; then
        echo -en "${ORCA}Um, I couldn't find a Docker-Compose project in this directory $(printf '\056%.0s' {1..100})" | head -c 100 ; echo -e "..... ${SORRY}"
        echo -e ${LINE}
        exit 1;
    else
        docker-compose restart
    fi
}


# === Stop all containers
logs() {
    if [ ! -f './docker-compose.yml' ]; then
        echo -en "${ORCA}Um, I couldn't find a Docker-Compose project in this directory $(printf '\056%.0s' {1..100})" | head -c 100 ; echo -e "..... ${SORRY}"
        echo -e ${LINE}
        exit 1;
    else
        docker-compose logs
    fi
}


# === Display help on using Orca
help() {
    echo -e " ⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀${CYB}⣶⣿${DEF}⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀  ${CYB}ORCA${DEF}"
    echo -e " ⠀⠀⠀⠀⠀⠀⠀⠀${CYB}⢀⣠⣾⣿⣿⠓⣒⣆⣤⣤⣤${DEF}⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀  Author: Prishalan Naidoo"
    echo -e " ⠀⠀⠀⠀${CYB}⣠⣶⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣷⣦⣄${DEF}⠀⠀⠀⠀⠀⠀⠀  ${BLD}Hey! I am your friendly neighbourhood docker-compose CLI helper,${DEF}"
    echo -e " ⠀⠀${CYB}⣴⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⠛⠛⠛⠻⣿⣿⣿⣿⣶⡀${DEF}⠀⠀⠀⠀  ${BLD}Orca. I can't help with everything, but I can help speed up your${DEF}"
    echo -e " ${CYB}⣠⣿⣿⠛⣿⣿⣿⣿⣿⣿⠿⠟⠛⠛⠋⠂⠀⠉⠉⠋⠛⠛⠿⢿⣿⣿⣷⡀${DEF}⠀⠀  ${BLD}docker tasks. With the exception of my 'bootstrap' method, you${DEF}"
    echo -e " ${CYB}⣿⣿⣤⠋⢋⠻⣿⣿⣷⣦⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠉⠻⣿⣴⡖${DEF}  ${BLD}will need to run me inside a docker-compose project directory.${DEF}"
    echo -e " ${CYB}⠿⠟⠀⠀⠀⠀⠀⠉⠉⠋⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⣿⡿⠁${DEF}  ${BLD}Here are some of my methods you can call on:${DEF}"
    echo ""
    printf "%b%-33s%b%-64s\n" "${YEB}" "orca help" "${DEF}"                     "Displays this help message."
    echo ""
    printf "%b%-33s%b%-64s\n" "${YEB}" "orca info" "${DEF}"                     "Displays useful info on your Docker-Compose containers."
    echo ""
    printf "%b%-33s%b%-64s\n" "${YEB}" "orca init" "${DEF}"                     "Initialises a docker-compose  skeleton project cloned from a Git"
    printf "%b%-33s%b%-64s\n" "${DEF}" "" "${DEF}"                              "repo -  by changing a few configuration file values, and adds an"
    printf "%b%-33s%b%-64s\n" "${DEF}" "" "${DEF}"                              "entry to your computer's hosts file. (Makes assumptions)"
    echo ""
    printf "%b%-33s%b%-64s\n" "${YEB}" "orca stats" "${DEF}"                    "Displays a live stream of computing resource usage statistics on"
    printf "%b%-33s%b%-64s\n" "${DEF}" "" "${DEF}"                              "the current running containers."
    echo ""
    printf "%b%-33s%b%-64s\n" "${YEB}" "orca ws [service-name] [args]" "${DEF}" "Issue CLI commands to a running container. You will need to pass"
    printf "%b%-33s%b%-64s\n" "${DEF}" "" "${DEF}"                              "in the name of the service as the first argument followed by the"
    printf "%b%-33s%b%-64s\n" "${DEF}" "" "${DEF}"                              "CLI commands as the subsequent arguments."
    printf "%b%-33s%b%-64s\n" "${DEF}" "" "${DEF}"                              "Example: orca ws workspace php -v"
    printf "%b%-33s%b%-64s\n" "${DEF}" "" "${DEF}"                              "(Hint: You can alias this command as well if it's easier)"
    echo ""
    printf "%b%-33s%b%-64s\n" "${YEB}" "orca bootstrap" "${DEF}"                "It works outside of a Docker-Compose project directory, and will"
    printf "%b%-33s%b%-64s\n" "${DEF}" "" "${DEF}"                              "clone a docker-compose skeleton from a Git repo into the current"
    printf "%b%-33s%b%-64s\n" "${DEF}" "" "${DEF}"                              "working directory, and run the 'init' method above.  This method"
    printf "%b%-33s%b%-64s\n" "${DEF}" "" "${DEF}"                              "makes some assumptions so please inspect my script to customize."
    echo ""
    printf "%b%-33s%b%-64s\n" "${YEB}" "orca start" "${DEF}"                    "Start all Docker-Compose containers in daemon mode."
    echo ""
    printf "%b%-33s%b%-64s\n" "${YEB}" "orca stop" "${DEF}"                     "Stop all Docker-Compose containers."
    echo ""
    printf "%b%-33s%b%-64s\n" "${YEB}" "orca restart" "${DEF}"                  "Restart all Docker-Compose containers."
    echo ""
    printf "%b%-33s%b%-64s\n" "${YEB}" "orca logs" "${DEF}"                     "View output from containers."
    echo ""
    echo -en "${ORCA}Have fun! $(printf '\056%.0s' {1..100})" | head -c 100 ; echo -e ". Bye ${HAPPY}"
    echo -e ${LINE}
    exit 0;
}


# ============= Script Entrypoint ================================================================
echo -e ${LINE}

if [ $# -eq 0 ]; then
    help
elif [[ $1 == bootstrap ]]; then
    bootstrap
elif [[ $1 =~ ^(help|info||init|stats|ws|start|stop|restart|logs)$ ]]; then
    # Check if docker compose file found in pwd, else, not docker-compose project
    if [ ! -f './docker-compose.yml' ]; then
        echo -en "${ORCA}Um, I couldn't find a Docker-Compose project in this directory $(printf '\056%.0s' {1..100})" | head -c 100 ; echo -e "..... ${SORRY}"
        echo -e ${LINE}
        exit 1;
    fi
    # Check if environment file exists in pwd, else, not docker-compose project
    if [ ! -f './.env' ]; then
        echo -en "${ORCA}Um, I couldn't find an environment file in this directory $(printf '\056%.0s' {1..100})" | head -c 100 ; echo -e "..... ${SORRY}"
        echo -e ${LINE}
        exit 1;
    fi
    # Final check to see if the docker-compose project has been initialised, else run method as per arg#1
    if [ -f './.needsinit' ]; then
        echo -e "${ORCA}Looks as though your project hasn't been initialised. Can I do this for you? (y/n) >"
        echo -en "${READ}"
        read doinit
        echo -e "${DEF}"
        if [[ $doinit =~ ^(y|Y)$ ]]; then
            echo -en "${ORCA}Ok cool, sorting out for you $(printf '\056%.0s' {1..100})" | head -c 100 ; echo -e "..... ${HAPPY}"
            init
        else
            echo -en "${ORCA}Fine, suit yourself then $(printf '\056%.0s' {1..100})" | head -c 100 ; echo -e ". Bye ${YEL}:|${DEF}"
            echo -e "${LINE}"
            exit 0;
        fi
    else
        "$@"
    fi
else 
    echo -en "${ORCA}Sorry, I don't know what '$@' is $(printf '\056%.0s' {1..100})" | head -c 100 ; echo -e ". Bye ${SORRY}"
    echo -e "${LINE}"
    exit 1
fi
