FROM php:7.1-apache

LABEL maintainer="Prishalan Naidoo <prishalann@digitalplanet.co.za>"

# Timezone variable for SAST
ENV TIMEZONE Africa/Johannesburg

# Set build args
ARG HOST_UID
ARG HOST_GID

# Install packages and php modules, and then clean up
RUN apt-get update \
    && apt-get install -y \
        nano \
        wget \
        mcrypt \
        tzdata \
        imagemagick \
        libmcrypt-dev \
        libicu-dev \
        libmagickwand-dev \
        libmagickcore-dev \
        libxslt-dev \
    && cp /usr/share/zoneinfo/${TIMEZONE} /etc/localtime \
    && echo "${TIMEZONE}" > /etc/timezone \
    && yes | pecl install \
        xdebug \
        imagick \
    && docker-php-ext-enable \
        xdebug \
        imagick \
    && docker-php-source delete \
    && docker-php-ext-configure gd \
        --with-gd \
        --with-mhash \
        --with-freetype-dir=/usr/include/ \
        --with-png-dir=/usr/include/ \
        --with-jpeg-dir=/usr/include/ \
    && NUMPROC=$(grep -c ^processor /proc/cpuinfo 2>/dev/null || 1) \
    && docker-php-ext-install -j${NUMPROC} \
        bcmath \
        gd \
        intl \
        zip \
        mcrypt \
        pdo_mysql \
        soap \
        xsl \
    && echo "zend_extension=$(find /usr/local/lib/php/extensions/ -name xdebug.so)" > /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini \
    && apt-get remove --purge -y \
        g++ \
        make \
        libtool \
        autoconf \
    && rm -rf \
        /tmp/* \
        /var/cache/apt/*

# Enable PHP modules
RUN cd /etc/apache2/mods-enabled/ \
    && ln -sf ../mods-available/rewrite.load \
    && ln -sf ../mods-available/session.load \
    && ln -sf ../mods-available/session_cookie.load \
    && ln -sf ../mods-available/session_crypto.load \
    && cd /

# Copy configurations into image
COPY ./build/start.sh /usr/local/bin/start

# Change Apache port
RUN sed -i "s/Listen 80/Listen 8080/g" /etc/apache2/ports.conf

# Create directories & set script permissions
RUN mkdir /mysqldumps \
    && chown -R www-data:www-data /mysqldumps \
    && chmod +x /usr/local/bin/start

# Add Composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

# Install PHPUnit (https://phpunit.de/getting-started.html)
RUN wget https://phar.phpunit.de/phpunit.phar \
    && chmod +x phpunit.phar \
    && mv phpunit.phar /usr/local/bin/phpunit

# Fix www-data user permissions
RUN usermod -u ${HOST_UID} www-data \
    && groupmod -g ${HOST_GID} www-data

# Expose ports
EXPOSE 8080