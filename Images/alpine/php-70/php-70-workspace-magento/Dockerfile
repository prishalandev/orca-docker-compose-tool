FROM php:7.0-cli-alpine

LABEL maintainer="Prishalan Naidoo <prishalann@digitalplanet.co.za>"

# Set build args
ARG HOST_UID
ARG HOST_GID

# Set build variables
ENV TIMEZONE Africa/Johannesburg
ENV BUILD_DEPS \
        g++ \
        make \
        pcre-dev \
        libtool \
        autoconf \
        tzdata
ENV PKGS_DEV \
        libxslt-dev \
        libmcrypt-dev \
        icu-dev \
        libxml2-dev \
        freetype-dev \
        libpng-dev \
        libjpeg-turbo-dev \
        imagemagick-dev
ENV PKGS_NORM \
        nano \
        nodejs@edge \
        zip \
        curl \
        openssl \
        openssh \
        libintl \
        libmcrypt \
        libltdl

# Install packages and php modules, set permissions, and then clean up
RUN echo "@edge http://dl-cdn.alpinelinux.org/alpine/edge/community" >> /etc/apk/repositories \
    && apk update \
    && apk upgrade \
    && apk add --no-cache --virtual \
        .build-deps \
        ${BUILD_DEPS} \
    && cp /usr/share/zoneinfo/${TIMEZONE} /etc/localtime \
    && echo "${TIMEZONE}" > /etc/timezone \
    && apk add --no-cache \
        ${PKGS_DEV} \
        ${PKGS_NORM} \
    && docker-php-ext-configure gd \
        --with-gd \
        --with-mhash \
        --with-freetype-dir=/usr/include/ \
        --with-png-dir=/usr/include/ \
        --with-jpeg-dir=/usr/include/ \
    && docker-php-source extract \
    && pecl install \
        imagick \
        redis-3.1.2 \
    && docker-php-ext-enable \
        imagick \
        redis \
    && docker-php-source delete \
    && NUMPROC=$(grep -c ^processor /proc/cpuinfo 2>/dev/null || 1) \
    && docker-php-ext-install -j${NUMPROC} \
        bcmath \
        gd \
        intl \
        zip \
        mcrypt \
        pdo_mysql \
        soap \
        xsl \
    # Fix www-data user permissions for host (local dev only)
    && apk --update add shadow@edge \
    && usermod -u ${HOST_UID} www-data \
    && groupmod -g ${HOST_GID} www-data \    
    # Clean up
    && apk del --no-cache \
        .build-deps \
        ${BUILD_DEPS} \
        shadow \
    && rm -rf \
        /usr/share/php7 \
        /tmp/* \
        /var/cache/apk/*

# Install composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

# Install PHPUnit (https://phpunit.de/getting-started.html)
RUN wget https://phar.phpunit.de/phpunit.phar \
    && chmod +x phpunit.phar \
    && mv phpunit.phar /usr/local/bin/phpunit

# Install N98-Magerun, N98-Magerun2, and Modman
RUN curl -O https://files.magerun.net/n98-magerun.phar \
    && chmod +x ./n98-magerun.phar \
    && mv ./n98-magerun.phar /usr/local/bin/magerun \
    && curl -O https://files.magerun.net/n98-magerun2.phar \
    && chmod +x ./n98-magerun2.phar \
    && mv ./n98-magerun2.phar /usr/local/bin/magerun2 \
    && curl -O https://raw.githubusercontent.com/colinmollenhour/modman/master/modman \
    && chmod +x ./modman \
    && mv ./modman /usr/local/bin/modman

# Expose ports
EXPOSE 9000

# Set Working directory
WORKDIR /var/www/html

# Opting not to set user. Default user will be 'root'. Use docker-compose exec -u www-data [container_name] [commands] ... OR ... docker run -u www-data
#USER www-data

# Set Command to keep container alive. Remove/comment this line for deployment environment
# CMD ["php", "-a"]
CMD ["tail", "-f", "/dev/null"]