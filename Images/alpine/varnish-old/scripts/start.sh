#!/bin/sh

set -e

varnishd -a 0.0.0.0:8080 -F -f $VCL_CONFIG -s malloc,$CACHE_SIZE -p default_ttl=3600 -p default_grace=3600 -T 0.0.0.0:6082 -S /etc/varnish/secret