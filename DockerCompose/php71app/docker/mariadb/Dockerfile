FROM alpine:3.6

LABEL maintainer="Prishalan Naidoo <prishalann@digitalplanet.co.za>"

# Set build args
ARG HOST_UID
ARG HOST_GID

# Set build variables
ENV TIMEZONE Africa/Johannesburg
ENV BUILD_DEPS \
		tzdata \
		shadow
ENV PKGS_NORM \
		mysql \
		mysql-client

# Install packages, set permissions, and then clean up
RUN apk update \
    && apk upgrade \
    && apk add --no-cache \
		${BUILD_DEPS} \
    && cp /usr/share/zoneinfo/${TIMEZONE} /etc/localtime \
    && echo "${TIMEZONE}" > /etc/timezone \
    && apk add --no-cache \
        ${PKGS_NORM} \
	# Change mysql user and group IDs to match host uid & gid (local dev only)
	&& usermod -u ${HOST_UID} mysql \
	&& groupmod -g ${HOST_GID} mysql \
	# Clean up
	&& apk del --no-cache \
		${BUILD_DEPS} \
	&& rm -rf \
		/tmp/* \
		/var/cache/apk/*

# Create volume for databases
VOLUME ["/var/lib/mysql"]

# Create directories
RUN mkdir /mysqldumps /scripts

# Copy startup script into image
COPY ./build/*.sh /scripts/
COPY ./build/my.cnf /etc/mysql/my.cnf

# Set script permissions and move scripts for global usage
RUN chmod +x /scripts/*.sh \
	&& mv /scripts/start.sh /start.sh \
	&& ln -s /scripts/mysqlimport.sh /usr/local/bin/mysqlimport \
	&& ln -s /scripts/mysqlexport.sh /usr/local/bin/mysqlexport

# Expose ports
EXPOSE 3306

# Set image entry-point
ENTRYPOINT ["/start.sh"]