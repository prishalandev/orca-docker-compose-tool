# DOCKER CONTAINERS FOR MAGENTO 1 #

This repo is used to create a ready-to-go, isolated set of container services on your local development computer to run a Magento 1 site. It comprisies the following containers for micro-services:

* Nginx
* PHP-7.0 (running as FPM)
* MariaDb 10
* Redis
* tianon/true container for storage persistence

---
## Getting Started ##
1. Ensure that Docker and Docker-Compose is installed on your local development machine.
2. Change to your project or sites directory and clone this repo into a specific folder name: **`git clone USERNAME@bitbucket.org:digitalplanetdevelopment/docker-magento1.git PROJECT_FOLDER_NAME`**, where `USERNAME` is your Git Username, and `PROJECT_FOLDER_NAME` is the name of the folder that you wish to be created and the repo to be cloned into.
Change directory into the newly created directory and run the following command to remove the Git references and placeholder files:
`rm -rf .git .gitignore www/.placeholder data/mysql/.placeholder data/mysqldumps/.placeholder data/redis/.placeholder`
3. Open the `.env` file in the repo root, and change:
COMPOSE_PROJECT_NAME
MYSQL_DATABASE
USER_UID --> get this by typing `id` in your terminal
USER_GID --> get this by typing `id` in your terminal
4. Open the `docker/nginx/configs/default.conf` file and change all instances of **CHANGEME** to an appropriate domain name.
5. Add your dev domain to your *hosts* file:
`sudo -- sh -c -e "echo '127.0.0.1     dev.YOUR_DOMAIN_NAME.local' >> /etc/hosts";`
6. The site will be available at *http://dev.YOUR_DOMAIN_NAME.local/*
7. Clone your Magento repo into the `www` folder
8. At the root of the docker directory, run: `docker-compose up -d`

---
## Although, before you start ... ##
* This docker project is **not** production-ready! Additional work is required to make the images and services used more portable, and staging or production ready.
* The only way to avoid any permissions issues you may experience,  is to match the respective conatiner service users (Php, Nginx, Mysql, etc) to your computer UID & GID. It is for this reason that this solution is not portable.
* You may want to consider building the containers before running them. This is a once-off: `docker-compose build`
* Be consistent in your naming conventions!

---
## Notes on Containers ##
#### PHP-FPM####
* Access the PHP container: `docker-compose exec phpfpm /bin/sh`
* All necessary PHP modules loaded
* Xdebug loaded on remote port 9000
* Composer is installed
#### Nginx ####
* Access the Nginx container: `docker-compose exec nginx /bin/sh`
#### MariaDB/Mysql ####
* Access the MySQL container: `docker-compose exec mysql /bin/sh`
* Run `mysqlexport` to export and gunzip the site database into the *data/mysqldumps* directory.
* Run `mysqlimport` to import an SQL file into the site database. This requires that the file is called **dbimport.sql.gz** and is located in the the *data/mysqldumps* directory.

#### Redis ####
* Access the Redis container: `docker-compose exec redis /bin/sh`
* redis-cli installed
* Ensure appropriate entries in the Magento local.xml to make use of or test Redis caching.

---
## To-Do ##
* Cron container
* RabbitMQ container
* Utility script to switch enviroments (localdev, staging, production)
* Supervisor configuration