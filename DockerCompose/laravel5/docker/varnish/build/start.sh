#!/bin/sh

set -e

echo "Starting Varnish"

varnishd \
    -a 0.0.0.0:8080 \
    -f /etc/varnish/default.vcl \
    -s malloc,$CACHE_SIZE \
    -F \
    -p default_ttl=3600 \
    -p default_grace=3600 \
    -T 0.0.0.0:6082