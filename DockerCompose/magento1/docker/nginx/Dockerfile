FROM nginx:1.12-alpine

LABEL maintainer="Prishalan Naidoo <prishalann@digitalplanet.co.za>"

# Set build args
ARG HOST_UID
ARG HOST_GID

# Set build variables
ENV TIMEZONE Africa/Johannesburg
ENV BUILD_DEPS \
        tzdata

# Install packages, set permissions, and then clean up
RUN apk update \
    && apk upgrade \
    && apk add --no-cache --virtual \
        ${BUILD_DEPS} \
    && cp /usr/share/zoneinfo/${TIMEZONE} /etc/localtime \
    && echo "${TIMEZONE}" > /etc/timezone \
    # Fix www-data user permissions for host (local dev only)
    && addgroup -g ${HOST_GID} -S www-data \
    && adduser -u ${HOST_UID} www-data -G www-data -H -s /bin/false -D \
    # Clean up
    && apk del --no-cache \
        ${BUILD_DEPS} \
    && rm -rf \
        /tmp/* \
        /var/cache/apk/*

# Remove default Nginx site config
RUN rm /etc/nginx/conf.d/default.conf

# Add modified nginx configuration
COPY ./build/nginx.conf /etc/nginx/nginx.conf

# Change ownerships
RUN touch /var/run/nginx.pid \
	&& chown -R www-data:www-data /var/run/nginx.pid \
	&& chown -R www-data:www-data /var/cache/nginx

# Set volume
VOLUME /var/www/html

# Set user
USER www-data